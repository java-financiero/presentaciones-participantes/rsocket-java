package io.pivotal.rsocketserver;

import io.pivotal.rsocketserver.data.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.annotation.ConnectMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import javax.annotation.PreDestroy;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class RSocketController {

     static final String SERVER = "Server";
     static final String RESPONSE = "Response";
     static final String STREAM = "Stream";
     static final String CHANNEL = "Channel";

    private final List<RSocketRequester> CLIENTS = new ArrayList<>();

    @PreDestroy
    void shutdown() {
       System.out.println("Detaching all remaining clients...");
        System.out.println();
        CLIENTS.stream().forEach(requester -> requester.rsocket().dispose());
       System.out.println("Shutting down.");
    }

    @ConnectMapping("shell-client")
    void connectShellClientAndAskForTelemetry(RSocketRequester requester, @Payload String client) {

        requester.rsocket()
                .onClose()
                .doFirst(() -> {
                    // Add all new clients to a client list
                   System.out.println("Client: "+ client+" CONNECTED.");
                    CLIENTS.add(requester);
                })
                .doOnError(error -> {
                    // Warn when channels are closed by clients
                   System.out.println("Channel to client  "+ client+" CLOSED");
                })
                .doFinally(consumer -> {
                    // Remove disconnected clients from the client list
                    CLIENTS.remove(requester);
                   System.out.println("Client  "+ client+"DISCONNECTED");
                })
                .subscribe();

        // Callback to client, confirming connection
        requester.route("client-status")
                .data("OPEN")
                .retrieveFlux(String.class)
                .subscribe();
    }

    /**
     * This @MessageMapping is intended to be used "request --> response" style.
     * For each Message received, a new Message is returned with ORIGIN=Server and INTERACTION=Request-Response.
     *
     * @param request
     * @return Message
     */
    @MessageMapping("request-response")
    String requestResponse(String  request) {
       System.out.println("Received request-response request: {}"+ request);
        // create a single Message and return it
        return "Received request-response request: "+ request;
    }

    /**
     * This @MessageMapping is intended to be used "fire --> forget" style.
     * When a new CommandRequest is received, nothing is returned (void)
     *
     * @param request
     * @return
     */
    @MessageMapping("fire-and-forget")
    public void fireAndForget(final String request) {
       System.out.println("Received fire-and-forget request: "+ request);
    }

    /**
     * This @MessageMapping is intended to be used "subscribe --> stream" style.
     * When a new request command is received, a new stream of events is started and returned to the client.
     *
     * @param request
     * @return
     */
    @MessageMapping("stream")
    Flux<Object> stream(final String request) {
       System.out.println("Received stream request: "+ request);

       return Flux
                // create a new indexed Flux emitting one element every second
                .interval(Duration.ofSeconds(1))
                // create a Flux of new Messages using the indexed Flux
                .map(index -> new Message(SERVER, STREAM, index));
    }

    /**
     * This @MessageMapping is intended to be used "stream <--> stream" style.
     * The incoming stream contains the interval settings (in seconds) for the outgoing stream of messages.
     *
     * @param settings
     * @return
     */
    @MessageMapping("channel")
    Flux<Object> channel(final Flux<Duration> settings) {
       System.out.println("Received channel request...");
        return settings
                .doOnNext(setting ->System.out.println("Channel frequency setting is"+setting.getSeconds()+" second(s)."))
                .doOnCancel(() ->System.out.println("The client cancelled the channel."))
                .switchMap(setting -> Flux.interval(setting)
                        .map(index -> new Message(SERVER, CHANNEL, index)));
    }
}
