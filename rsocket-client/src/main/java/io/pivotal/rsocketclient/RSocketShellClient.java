package io.pivotal.rsocketclient;


import io.pivotal.rsocketclient.data.Message;
import io.rsocket.SocketAcceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PreDestroy;
import java.nio.channels.ClosedChannelException;
import java.time.Duration;
import java.util.UUID;

@Slf4j
@ShellComponent
public class RSocketShellClient {

    private final RSocketRequester rsocketRequester;
    private static final String CLIENT = "Client";
    private static final String REQUEST = "Request";
    private static final String FIRE_AND_FORGET = "Fire-And-Forget";
    private static final String STREAM = "Stream";
    private static Disposable disposable;

    @Autowired
    public RSocketShellClient(RSocketRequester.Builder rsocketRequesterBuilder, RSocketStrategies strategies) {
        String client = UUID.randomUUID().toString();
       System.out.println("Connecting using client ID: "+ client);

        SocketAcceptor responder = RSocketMessageHandler.responder(strategies, new ClientHandler());

        this.rsocketRequester = rsocketRequesterBuilder
                .setupRoute("shell-client")
                .setupData(client)
                .rsocketStrategies(strategies)
                .rsocketConnector(connector -> connector.acceptor(responder))
                .connectTcp("localhost", 7000)
                .block();


        this.rsocketRequester.rsocket()
                .onClose()
                .doOnError(error ->System.out.println("Connection CLOSED"))
                .doFinally(consumer ->System.out.println("Client DISCONNECTED"))
                .subscribe();
    }

    @PreDestroy
    void shutdown() {
        rsocketRequester.rsocket().dispose();
    }

    @ShellMethod("Send one request. One response will be printed.")
    public void requestResponse() throws InterruptedException {
       System.out.println("\nSending one request. Waiting for one response...");
       Message m =new Message(CLIENT, REQUEST);
       System.out.println(m.toString());
        String message = this.rsocketRequester
                .route("request-response")
                .data(m.toString())
                .retrieveMono(String.class)
                .block();
       System.out.println("\nResponse was: "+ message);
    }

    @ShellMethod("Send one request. No response will be returned.")
    public void fireAndForget() throws InterruptedException {
       System.out.println("\nFire-And-Forget. Sending one request. Expect no response (check server console log)...");
       Message m =new Message(CLIENT, REQUEST);
       this.rsocketRequester
                .route("fire-and-forget")
                .data(m.toString())
                .send()
                .block();
    }

    @ShellMethod("Send one request. Many responses (stream) will be printed.")
    public void stream() {
       System.out.println("\n\n**** Request-Stream\n**** Send one request.\n**** Log responses.\n**** Type 's' to stop.");
       Message m =new Message(CLIENT, REQUEST);
       disposable = this.rsocketRequester
                .route("stream")
                .data(m.toString())
                .retrieveFlux(String.class)
                .subscribe(message ->System.out.println("Response: "+message+" (Type 's' to stop.)" ));
    }

    @ShellMethod("Stream some settings to the server. Stream of responses will be printed.")
    public void channel() {
       System.out.println("\n\n***** Channel (bi-directional streams)\n***** Asking for a stream of messages.\n***** Type 's' to stop.\n\n");

        Mono<Duration> setting1 = Mono.just(Duration.ofSeconds(1));
        Mono<Duration> setting2 = Mono.just(Duration.ofSeconds(3)).delayElement(Duration.ofSeconds(5));
        Mono<Duration> setting3 = Mono.just(Duration.ofSeconds(5)).delayElement(Duration.ofSeconds(15));

        Flux<Duration> settings = Flux.concat(setting1, setting2, setting3)
                .doOnNext(d ->System.out.println("\nSending setting for a "+ d.getSeconds()+"-second interval.\n"));

        disposable = this.rsocketRequester
                .route("channel")
                .data(settings)
                .retrieveFlux(String.class)
                .subscribe(message ->System.out.println("Received: "+message+" \n(Type 's' to stop."));
    }

    @ShellMethod("Stops Streams or Channels.")
    public void s() {
        if (null != disposable) {
           System.out.println("Stopping the incoming stream.");
            disposable.dispose();
           System.out.println("Stream stopped.");
        }
    }


}

@Slf4j
class ClientHandler {

    @MessageMapping("client-status")
    public Flux<String> statusUpdate(String status) {
       System.out.println("Connection {}"+ status);
        //return Mono.just(System.getProperty("java.vendor") + " v" + System.getProperty("java.version"));
        return Flux.interval(Duration.ofSeconds(5)).map(index -> String.valueOf(Runtime.getRuntime().freeMemory()));
    }
}
